package com.yb.boot.security.jwt.common.common;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.collections.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Description: 登录用户信息封装类
 * 这个空参数构造是必要的,如果不提供,将导致jwt解析封装的时候无法封装字段(实测因为添加了有参构造而没有空参构造导致)@NoArgsConstructor
 * @author biaoyang
 * date 2019/4/25 002511:10
 */
@Getter
@Setter
@NoArgsConstructor
public class LoginUser {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名
     */
    @JSONField(name = "sub")
    private String username;

    /**
     * 用户姓名
     */
    private String fullName;

    /**
     * 用户身份证号
     */
    private String idCard;

    /**
     * 手机号
     */
    private String cellphone;

    /**
     * 机构代码
     */
    private Long orgCode;

    /**
     * 机构名称(部门)
     */
    private String orgName;

    /**
     * 角色
     */
    private Set<String> roles;

    /**
     * 权限
     */
    private Set<String> perms;

    /**
     * 用户ip
     */
    private String ip;

    /**
     * 用户请求uri
     */
    private String uri;

    /**
     * 登录终端
     */
    private String from;

    /**
     * jti 唯一代码
     */
    private String jti;

    /**
     * 认证状态：未认证,认证中,通过,不通过auth_status
     */
    private String authStatus;

    /**
     * 因为这个肯定需要的,其他的就在这个基础上set即可(因为无法使用无参构造)
     * @param username
     * @param roles
     * @param perms
     */
    public LoginUser(String username, Set<String> roles, Set<String> perms) {
        this.username = username;
        this.roles = roles;
        this.perms = perms;
    }

    /**
     * 因为这个肯定需要的,其他的就在这个基础上set即可(因为无法使用无参构造)
     * 因为很多时候权限角色没有使用set集合,而是list集合,这里为了构造时避免转换导致代码太长
     * @param username
     * @param roles
     * @param perms
     */
    public LoginUser(String username, List<String> roles, List<String> perms) {
        this.username = username;
        this.roles = CollectionUtils.isNotEmpty(roles)?new HashSet<>(roles):null;
        this.perms =  CollectionUtils.isNotEmpty(perms)?new HashSet<>(perms):null;
    }

}
