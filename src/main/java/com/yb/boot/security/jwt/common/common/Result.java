package com.yb.boot.security.jwt.common.common;

import lombok.Data;

/**
 * @author yangbiao
 * @Description: 这里是restful风格,返回数据封装类,为了方便,可以直接传入Object,
 * 而不是具体的类型例如Result<Object>就可以接受各种返回类型,就不会每次都去更改了
 * @date 2018/9/28
 */
@Data
public class Result<T> {
    private static final String MESSAGE_SUCCESS = "success";
    private static final Integer STATUS_SUCCESS = 200;
    private static final Integer STATUS_ERROR = 400;

    private Integer status;
    private String message;
    private T data;

    /**
     * 供接口返回数据使用
     *
     * @param t
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(T t) {
        Result<T> result = new Result<>();
        result.setStatus(STATUS_SUCCESS);
        result.setMessage(MESSAGE_SUCCESS);
        result.setData(t);
        return result;
    }

    /**
     * 这个一般用不着,因为基本都是用异常来捕捉的
     *
     * @param message
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(String message) {
        Result<T> result = new Result<>();
        result.setStatus(STATUS_ERROR);
        result.setMessage(message);
        return result;
    }

    /**
     * 这个主要用来在异常捕捉类里简化返回数据的封装
     *
     * @param status
     * @param <T>
     * @return
     */
    public static <T> Result<T> withStatus(Integer status) {
        Result<T> result = new Result<>();
        result.setStatus(status);
        return result;
    }

    /**
     * 将传入的信息设置到成员变量并返回对象
     * @param message
     * @return
     */
    public Result withMessage(String message) {
        this.message = message;
        return this;
    }

}
