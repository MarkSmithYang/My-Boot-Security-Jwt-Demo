package com.yb.boot.security.jwt.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;

import java.io.Serializable;

/**
 * @author biaoyang
 */
@Configuration
@AllArgsConstructor
public class RedisConfig {

    private final RedisConnectionFactory redisConnectionFactory;

    /**
     * 注意这里设置的redis的序列化需要和网关使用的redisTemplate的序列化保持一致
     * 实测,如果存储和获取使用的redisTemplate的序列化方式不一致,将导致获取失败
     * @return
     */
    @Bean
    public RedisTemplate<String, Serializable> redisTemplate() {
        RedisTemplate<String, Serializable> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        redisTemplate.setKeySerializer(redisTemplate.getStringSerializer());
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        return redisTemplate;
    }
}
